package com.autotube.autotube_ui;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

public class AutotubeUI extends Application {
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) { }

        @Override
        public void onServiceDisconnected(ComponentName name) { }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        bindService(new Intent(this, NetworkService.class), connection, BIND_AUTO_CREATE);
    }
}
