package com.autotube.autotube_ui;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements NetworkService.Listener {
    private NetworkService network;
    private boolean bound = false;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public void onServerAdded(NetworkService.ServerData serverData) {
        // pass
    }

    @Override
    public void onServerRemoved(NetworkService.ServerData serverData) {
        // pass
    }

    @Override
    public void onServerSelected(NetworkService.ServerData serverData) {
        final NetworkService.ServerData data = serverData;
        runOnUiThread(new Runnable() {
            public void run() {
                TextView channelText = (TextView) MainActivity.this.findViewById(R.id.channelText);
                channelText.setVisibility(View.GONE);

                TextView statusText = (TextView) findViewById(R.id.statusText);
                View controls = findViewById(R.id.layoutControls);

                if (data.isEmpty()) {
                    statusText.setText(getText(R.string.status_no_device_found));
                    controls.setVisibility(View.GONE);
                } else {
                    statusText.setText(data.name);
                    controls.setVisibility(View.VISIBLE);
                    askChannelText();
                }
            }
        });
    }

    private void askChannelText() {
        ResponseHandler handler = new ResponseHandler() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        TextView channelText = (TextView) MainActivity.this.findViewById(R.id.channelText);
                        channelText.setText(response);
                        channelText.setVisibility(View.VISIBLE);
                    }
                });
            }
        };

        new RequestTask(new Request(handler, "channel", "current_name")).execute();

        Log.d("MESSAGES", "Asking channel name");
        Log.d("MESSAGES", Arrays.toString(Thread.currentThread().getStackTrace()));
    }

    public void onNextChannel(View v) {
        new RequestTask(new Request("channel", "next")).execute();
        askChannelText();
    }

    public void onPreviousChannel(View v) {
        new RequestTask(new Request("channel", "previous")).execute();
        askChannelText();
    }

    public void onNextClip(View v) {
        new RequestTask(new Request("channel", "next_clip")).execute();
    }

    public void onPreviousClip(View v) {
        new RequestTask(new Request("channel", "previous_clip")).execute();
    }

    public void onCloseProgram(View v) {
        new RequestTask(new Request("autotube", "stop")).execute();
        finish();
    }

    public void onVolumeUp(View v) {
        new RequestTask(new Request("channel", "volume_up")).execute();
    }

    public void onVolumeDown(View v) {
        new RequestTask(new Request("channel", "volume_down")).execute();
    }

    public void onSeekLeft(View v) {
        new RequestTask(new Request("channel", "seek_left")).execute();
    }

    public void onSeekRight(View v) {
        new RequestTask(new Request("channel", "seek_right")).execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        TextView statusText = (TextView) findViewById(R.id.statusText);
        LinearLayout controls = (LinearLayout) findViewById(R.id.layoutControls);

        statusText.setText(getText(R.string.status_no_device_found));
        controls.setVisibility(View.GONE);

        bindService(new Intent(this, NetworkService.class), connection, BIND_AUTO_CREATE);
        Log.d("MESSAGES", "Bound to service");
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (bound) {
            network.pause();
            network.removeListener(MainActivity.this);
            unbindService(connection);
            bound = false;
            Log.d("MESSAGES", "Unbound from service");
        }
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d("MESSAGES", "On service connected");

            NetworkService.ServiceBinder binder = (NetworkService.ServiceBinder) service;
            network = binder.getService();
            network.addListener(MainActivity.this);
            network.resume();
            bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    // AsyncTask for tasks that can block
    private class RequestTask extends AsyncTask<Void, Void, Void> {
        Request request;

        public RequestTask(Request request) {
            this.request = request;
        }

        @Override
        protected Void doInBackground(Void... params) {
            network.addRequest(request);
            return null;
        }
    }
}
