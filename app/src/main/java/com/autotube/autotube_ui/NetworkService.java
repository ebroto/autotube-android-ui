package com.autotube.autotube_ui;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by edu on 16/03/16.
 */
public class NetworkService extends Service {
    public static final int DISCOVERY_PORT = 47318; // #TODO: remove. Find correct way of handling default values. Allow to change value from settings.
    public static final int MAX_RECV_SIZE = 2048;
    public static final int DISCOVER_SERVER_TIMEOUT = 30;
    public static final int PING_SERVER_TIMEOUT = 10;
    public static final int PING_SERVER_DELTA = 3;

    private final IBinder binder;
    private ServiceThread serviceThread;
    private List<Request> requests;
    private List<Listener> listeners;
    private Server server;
    private Discovery discovery;

    public NetworkService() throws SocketException {
        binder = new ServiceBinder();
        serviceThread = new ServiceThread();
        requests = new ArrayList<>();
        listeners = new ArrayList<>();
        server = new Server();
        discovery = new Discovery();
    }

    @Override
    public void onCreate() {
        serviceThread.start();
        Log.d("MESSAGES", "Service created.");
    }

    @Override
    public void onDestroy() {
        if (serviceThread.isAlive()) {
            serviceThread.terminateService();
            try {
                serviceThread.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        Log.d("MESSAGES", "Service destroyed.");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void pause() {
        if (serviceThread.isAlive()) {
            serviceThread.pauseService();
        }
    }

    public void resume() {
        if (serviceThread.isAlive()) {
            serviceThread.resumeService();
        }
    }

    public void addListener(Listener listener) {
        synchronized (listeners) {
            listeners.add(listener);
        }
    }

    public void removeListener(Listener listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    public void addRequest(Request request) {
        if (server.isConnected()) {
            synchronized (requests) {
                requests.add(request);
            }
        }
    }

    public class ServiceBinder extends Binder {
        NetworkService getService() {
            return NetworkService.this;
        }
    }

    public class ServerData {
        public final String name;
        public final String ip;
        public final int port;

        public ServerData() {
            name = "";
            ip = "";
            port = 0;
        }

        public ServerData(String name, String ip, int port) {
            this.name = name;
            this.ip = ip;
            this.port = port;
        }

        public ServerData(ServerData other) {
            this(other.name, other.ip, other.port);
        }

        public boolean isEmpty() {
            return name.isEmpty() || ip.isEmpty() || port == 0;
        }

        @Override
        public boolean equals(Object other) {
            if (other == null) return false;
            if (other == this) return true;
            if (!(other instanceof ServerData)) return false;

            ServerData otherData = (ServerData)other;
            return name.equals(otherData.name) && ip.equals(otherData.ip) && port == otherData.port;
        }

        @Override
        public String toString() {
            return name + "," + ip + "," + port;
        }
    }

    public interface Listener {
        void onServerAdded(ServerData serverData);
        void onServerRemoved(ServerData serverData);
        void onServerSelected(ServerData serverData);
    }

    private class ServiceThread extends Thread {
        private volatile boolean running = false;
        private volatile boolean paused = false;
        private volatile boolean doPause = false;
        private volatile boolean doResume = false;

        public void run() {
            running = true;
            paused = true;
            ServerData pausedServer = new ServerData();

            // Main service bucle.
            while (running) {
                if (paused) {
                    if (doResume) {
                        if (!pausedServer.isEmpty()) {
                            server.connect(pausedServer);
                            pausedServer = new ServerData();
                        }

                        doResume = false;
                        paused = false;
                        Log.d("MESSAGES", "Service resumed.");
                    } else {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            continue;
                        }
                    }
                } else {
                    if (doPause) {
                        if (server.isConnected()) {
                            server.closeConnection();
                            pausedServer = server.getPeerAddress();
                        }

                        doPause = false;
                        paused = true;
                        Log.d("MESSAGES", "Service paused.");
                    } else {
                        discovery.update();

                        if (server.isConnected()) {
                            server.update();
                        } else {
                            ServerData serverData = discovery.getNextServer();

                            if (!serverData.isEmpty()) {
                                server.connect(serverData);
                            }
                        }
                    }
                }
            }

            // Cleanup.
            discovery.close();

            if (server.isConnected()) {
                server.closeConnection();
            }
        }

        public void pauseService() {
            if (!paused) {
                doPause = true;
            }
        }

        public void resumeService() {
            if (paused) {
                doResume = true;
            }
        }

        public void terminateService() {
            running = false;
        }
    }

    private class ServerDataTimeout extends ServerData {
        public ServerDataTimeout(String name, String ip, int port) {
            super(name, ip, port);
            time = Utils.getTime();
        }

        public ServerDataTimeout(ServerData other) {
            this(other.name, other.ip, other.port);
        }

        public void update() {
            time = Utils.getTime();
        }

        public boolean timeoutReached() {
            return Utils.getTime() - time > DISCOVER_SERVER_TIMEOUT;
        }

        // not overriding "equals" intentionally
        // not overriding "toString" intentionally

        private long time;
    }

    private class Server {
        private ServerData data;
        private Socket sock;
        private long lastPing;
        private long sendPing;
        private volatile boolean connected;

        public Server() {
            clear();
        }

        public Server(ServerData serverData) {
            connect(serverData);
        }

        public boolean isConnected() {
            return connected;
        }

        public ServerData getPeerAddress() {
            return data;
        }

        public void connect(ServerData serverData) {
            closeConnection();

            if (!serverData.isEmpty()) {
                data = serverData;
                sock = null;

                try {
                    sock = new Socket(data.ip, data.port);
                    sock.setSoTimeout(50);
                    connected = true;

                    synchronized (listeners) {
                        Log.d("MESSAGES", "listeners.size():" + listeners.size());
                        for (Listener listener : listeners) {
                            Log.d("MESSAGES", "Calling on server selected 2");
                            listener.onServerSelected(serverData);
                        }
                    }

                    Log.d("MESSAGES", "Connection established: " + data.toString());

                } catch (ConnectException e) {
                    Log.d("MESSAGES", "Unable to connect to " + data.toString());
                    closeConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                    closeConnection();
                }
            }
        }

        public void closeConnection() {
            if (sock != null) {
                try {
                    sock.close();
                } catch (IOException e1) {
                    Log.e("MESSAGES", "Error closing socket");
                    e1.printStackTrace();
                }

                synchronized (requests) {
                    for (Request request : requests) {
                        request.cancel();
                    }

                    requests.clear();
                }

                synchronized (listeners) {
                    Log.d("MESSAGES", "listeners.size():" + listeners.size());
                    for (Listener listener : listeners) {
                        Log.d("MESSAGES", "Calling on server selected 1");
                        listener.onServerSelected(new ServerData());
                    }
                }

                Log.d("MESSAGES", "Connection closed: " + data.toString());
            }

            clear();
        }

        public void update() {
            if (isConnected()) {
                processRequest();
                checkTimeout();
            }
        }

        private void processRequest() {
            Request request = null;

            synchronized (requests) {
                if (!requests.isEmpty()) {
                    request = requests.get(0);
                    requests.remove(0);
                }
            }

            if (request != null) {
                request.process(sock);

                if (request.hasError()) {
                    closeConnection();
                } else if (!request.isDone()) {
                    synchronized (requests) {
                        requests.add(0, request);
                    }
                }
            }
        }

        private void checkTimeout() {
            if (connected) {
                long currentTime = Utils.getTime();

                if (currentTime - this.sendPing > PING_SERVER_DELTA) {
                    ResponseHandler responseHandler = new ResponseHandler() {
                        public void run() {
                            lastPing = Utils.getTime();
                        }
                    };

                    addRequest(new Request(responseHandler, "interface", "ping"));
                    this.sendPing = currentTime;
                }

                if (currentTime - this.lastPing > PING_SERVER_TIMEOUT) {
                    Log.d("MESSAGES", "Ping timeout");
                    closeConnection();
                }
            }
        }

        private void clear() {
            data = new ServerData();
            sock = null;
            connected = false;
            sendPing = Utils.getTime();
            lastPing = this.sendPing;
        }
    }

    private class Discovery {
        private List<ServerDataTimeout> availableServers;
        private DatagramSocket dsocket;
        DatagramPacket packet;
        private byte[] recvBuffer;
        private HashMap<InetAddress, String> rawData;

        public Discovery() throws SocketException {
            availableServers = new ArrayList<>();
            dsocket = new DatagramSocket(DISCOVERY_PORT);
            recvBuffer = new byte[MAX_RECV_SIZE];
            packet = new DatagramPacket(recvBuffer, recvBuffer.length);
            rawData = new HashMap<>();

            dsocket.setSoTimeout(50);
        }

        public void close() {
            if (dsocket != null) {
                dsocket.close();
                dsocket = null;
            }
        }

        public ServerData getNextServer() {
            ServerData server = new ServerData();

            if (!availableServers.isEmpty()) {
                server = availableServers.get(0);
            }

            return server;
        }

        public void update() {
            handleMessages();
            checkDeadServers();
        }

        private void handleMessages() {
            // Handle messages from selected server.

            if (dsocket != null) {
                try {
                    dsocket.receive(packet); // throws if timeout is reached
                    processData(packet.getAddress(), new String(recvBuffer, 0, packet.getLength()));
                } catch (SocketTimeoutException e) {
                    // pass
                } catch (IOException e) {
                    e.printStackTrace();
                    close();
                }
            }
        }

        private void processData(InetAddress address, String raw) {
            // Process raw data received. Messages could have been split into parts.

            String addrData = rawData.get(address);

            if (addrData == null) {
                addrData = raw;
                rawData.put(address, addrData);
            } else {
                addrData = addrData.concat(raw);
            }

            if (addrData.length() <= MAX_RECV_SIZE) {
                String message = "";
                int firstIndex = addrData.indexOf('|');

                if (firstIndex != -1) {
                    int secondIndex = addrData.indexOf('|', firstIndex + 1);

                    if (secondIndex != -1) {
                        message = addrData.substring(firstIndex + 1, secondIndex);
                        addrData = addrData.substring(secondIndex + 1);

                        if (addrData.isEmpty()) {
                            rawData.remove(address);
                        } else {
                            rawData.put(address, addrData);
                        }
                    }
                }

                if (!message.isEmpty()) {
                    processMessage(address, message);
                }
            } else {
                Log.e("MESSAGES", "Max packet size reached for " + address.toString() + ". Clearing buffer.");
                rawData.remove(address);
            }
        }

        private void processMessage(InetAddress address, String message) {
            // Process a complete message.

            String[] dataStr = message.split(",");

            if (dataStr.length == 2) {
                final ServerData serverData = new ServerData(
                        dataStr[0],
                        address.toString().replaceAll("^/", ""),
                        Integer.parseInt(dataStr[1]));

                if (!serverData.isEmpty()) {
                    ServerDataTimeout serverTimeout = new ServerDataTimeout(serverData);
                    int serverIndex = availableServers.indexOf(serverTimeout);

                    if (serverIndex == -1) {
                        availableServers.add(serverTimeout);
                        synchronized (listeners) {
                            for (Listener listener : listeners) {
                                listener.onServerAdded(serverData);
                            }
                        }
                    } else {
                        this.availableServers.get(serverIndex).update();
                    }
                }
            }
        }

        private void checkDeadServers() {
            // Remove a server if we no longer receive beacon packets.

            for (Iterator<ServerDataTimeout> iterator = availableServers.iterator(); iterator.hasNext(); ) {
                ServerDataTimeout server = iterator.next();

                if (server.timeoutReached()) {
                    synchronized (listeners) {
                        for (Listener listener : listeners) {
                            listener.onServerRemoved(server);
                        }
                    }

                    iterator.remove();
                }
            }
        }
    }
}
