package com.autotube.autotube_ui;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Arrays;

public class Request {
    private String requestStr;
    private ResponseHandler onResponse;
    private boolean hasSent;
    private boolean hasReceived;
    private boolean error;
    byte[] responseBytes;

    Request(ResponseHandler onResponse, String... parts) {
        StringBuilder builder = new StringBuilder();
        for (int partIndex = 0; partIndex < parts.length; partIndex++) {
            builder.append(parts[partIndex]);
            builder.append("\n");
        }

        this.requestStr = builder.toString();
        this.onResponse = onResponse;
        this.hasSent = false;
        this.hasReceived = false;
        this.error = false;

        if (this.onResponse != null) {
            this.responseBytes = new byte[NetworkService.MAX_RECV_SIZE];
        }
    }

    Request(String... parts) {
        this(null, parts);
    }

    public void process(Socket s) {
        send(s);
        receive(s);
    }

    public void reset() {
        hasSent = hasReceived = false;
    }

    public void cancel() {
        hasSent = hasReceived = true;
    }

    private void send(Socket sock) {
        if (!hasSent) {
            if (!requestStr.isEmpty()) {
                OutputStream output = null;

                try {
                    output = sock.getOutputStream();
                    output.write(requestStr.getBytes());
                    output.flush();
                    hasSent = true;

                    //#todo: remove log
                    if (!requestStr.equals("interface\nping\n")) {
                        Log.d("MESSAGES", "Message sent: " + requestStr);
                    }
                } catch (SocketException e) {
                    Log.d("MESSAGES", "Broken pipe");
                    cancel();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                hasSent = true;
            }
        }
    }

    private void receive(Socket sock) {
        if (!hasReceived) {
            if (onResponse != null) {
                InputStream input = null;
                try {
                    input = sock.getInputStream();
                    int recvBytes = 0;

                    try {
                        recvBytes = input.read(responseBytes);
                    } catch (SocketTimeoutException e) {
                        // pass
                    }

                    // #todo: handle segmented messages
                    if (recvBytes == -1) {
                        error = true;
                    } else if (recvBytes > 0) {
                        byte[] responseSliced = Arrays.copyOfRange(responseBytes, 0, recvBytes);
                        String recvData = new String(responseSliced, "UTF-8");
                        hasReceived = true;

                        //#todo: remove log
                        if (!recvData.equals("pong")) {
                            Log.d("MESSAGES", "Message received: " + recvData);
                        }

                        onResponse.setResponse(recvData);
                        onResponse.run();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                hasReceived = true;
            }
        }
    }

    public boolean isDone() {
        return hasSent && hasReceived;
    }

    public boolean hasError() {
        return error;
    }
}