package com.autotube.autotube_ui;

public abstract class ResponseHandler implements Runnable {
    void setResponse(String response) {
        this.response = response;
    }

    String response;
}
